require 'oauth2'
require 'json'
require 'net/http'
require 'uri'

class SalesforceApiController < ApplicationController
  CLIENT_ID = "3MVG9ZL0ppGP5UrDJTwC3UbTrYqoQBMyVEH14jM082IPttbItgB2ntdviAEtf2E_3zhqtsKON4PKCOofcV_oO"
  CLIENT_SECRET = "6689213292956213252"
  def index
    @access_token = session[:access_token]
  end

  def callback
    code = params[:code]
    uri = URI.parse("https://login.salesforce.com/services/oauth2/token")
    http = Net::HTTP::new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri, { "content_tpe" => "application/x-www-form-urlencoded" })
    authenticate_data = {"code" => code,
                         "grant_type" => "authorization_code",
                         "client_id" => CLIENT_ID,
                         "client_secret" => CLIENT_SECRET,
                         "redirect_uri" => "https://localhost:3000/salesforce_api/callback"}
    request.set_form_data(authenticate_data) #convert hash to form data
    response = http.request(request)
    @access_token = JSON.parse(response.body)["access_token"] || ""
    session[:access_token] = @access_token
    redirect_to :action => 'index'
  end
end
