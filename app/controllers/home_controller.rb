class HomeController < ApplicationController
  def index
    @lang = params[:lang] || 'en'
    @articles = Article.where(:lang => @lang)
  end
end
