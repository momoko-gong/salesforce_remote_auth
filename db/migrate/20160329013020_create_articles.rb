class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :text
      t.string :tag
      t.string :lang
      t.timestamps null: false
    end
  end
end
