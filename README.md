# Caution!
salesforce application don't allow http request as a redirect callback,
you have to run
1. `openssl x509 -req -days 365 -in server.csr -signkey server.key -out server.crt` to create a SSL certificate.

2. `bundle install` (This is based in Rails 4.2, Ruby 2.2.3)

3. `thin start --ssl  --ssl-key-file server.key --ssl-cert-file server.crt` to start https local server


# get access token through OAuth authorization
1. go to https://localhost:3000/salesforce_api/index , click the button
2. login in salesforce
3. access_token is set to session, if you delete session, you will need to login again
